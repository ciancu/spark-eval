%\eject
\section{Discussion}


Metadata latency and its relative lack of scalability is a problem common
to other~\cite{alam_parallel_2011,carns_small-file_2009} parallel
file systems used in HPC installations. The shuffle stage is at worst quadratic with cores in file open
operations, thus metadata latency can dominate Spark performance.  We believe our findings
to be of interest to more than Cray with Lustre HPC users and
operators. While Spark requires file I/O only for the shuffle phase,
Hadoop requires file I/O for both map and reduce phases and
also suffers from poor performance when run without local storage~\cite{sparks_cray_15}.
Our techniques may therefore also be applicable to Hadoop on HPC systems.

The hardware roadmap points towards improved performance and
scalability. Better MDS hardware improves  baseline performance (per
operation latency), as illustrated by the differences between Edison
and Cori. Multiple MDSes will  improve scalability. 
The current usage of \bb\ I/O acceleration on Cori, while it degrades
baseline node performance, it improves scalability up to thousands of cores. Better
performance from it can be expected shortly, as the next stage on  the
Cori software roadmap provides a caching mode for \bb\ which may
alleviate some of the current latency problems.
It may be the case
that the \bb\ is too far from the main memory, or that it is shared by
too many nodes for scales beyond $O(10^3)$.
The HPC node hardware evolution points towards large NVRAM deployed
inside the nodes, which should provide scalability with no capacity constraints.

As our evaluation has shown, software approaches can definitely
improve performance and scalability. Besides ours, there are several
other efforts with direct bearing. Deploying Spark on
Tachyon~\cite{tachyon} with support for hierarchical storage will
eliminate metadata operations. In fact, we have considered this option
ourselves but at the time of the writing the current release of Tachyon, 0.8.2, does not fully
support hierarchical storage  (missing append).  We expect its performance to fall in
between that of our configuration with a local file system backed by
Lustre and \rdisk+\fpool. Note also that our findings in
Section~\ref{sec:iterative}  about the
necessity of improving block management during the shuffle stage for
iterative algorithms are directly applicable to Tachyon. 

The Lustre roadmap also contains a shift to
object based storage with local metadata. Meanwhile, developers~\cite{hal,sparks_cray_15} have
already started writing and tuning HDFS emulators for Lustre. The
initial results are not encouraging and Lustre is faster than the HDFS
emulator.  We believe that the \lfs\  is the proper configuration for scalability.

The performance improvements due to \fpool\ when using ``local'' file
systems surprised us. This may come from the different kernel on the
Cray compute nodes, or it may be a common trait when running in data
center settings. As HPC workloads are not system call intensive, the
compute node kernels such as Cray CNL may not be fully optimized for
them. Running commercial data analytics workloads on HPC hardware may
force the community to revisit this decision.  It is definitely worth investigating  system calls
overhead and plugging in user level services (e.g. file systems) on
commercial clouds. 

Luu et al~\cite{Luu:2015} discuss the performance of HPC applications
based on six years of logs obtained from three supercomputing centers,
including on Edison. Their evaluation indicates that there is
commonality with the Spark behavior:  HPC applications tend to spend
40\% of their I/O  time in metadata operations than in data access and they tend to
use small data blocks. The magnitude of these operations in data
analytics workloads should provide even more incentive to system
developers to mitigate this overhead. 



We are interested in extending
this study with a comparison with Amazon EC2 to gain more quantitative
insights into the performance differences between systems with node
attached storage and network attached storage.   Without the
optimizations suggested in this paper, the comparison would have
favored data center architectures: low disk latency provides better
node performance and masks the deficiencies in support for iterative
algorithms. With our optimizations(\fpool+\lfs), single node HPC performance
becomes comparable and we can set to answer the question of the influence of system
design and software configuration on scalability. We believe that we
may have reached close to the point where horizontal data movement
dominates in the HPC installations as well. Such a comparison can guide both
system and software designers whether throughput optimizations in
large installations need to be supplemented with latency optimization
in order to support data analytics frameworks. 







