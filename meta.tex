\subsection{Impact of Metadata Access Latency on Scalability}
\label{sec:meta}

\begin{figure*}[!htb]
\begin{tabular}{cc}
\begin{minipage}{3.5in}
\centerline{\includegraphics[keepaspectratio,height=1.8in]{groupby-node-cori}}
\end{minipage} &
\begin{minipage}{3.5in}
\centerline{\includegraphics[keepaspectratio,height=1.8in]{pagerank-node-cori}}
\end{minipage} \\
\end{tabular}
%PageRank mounted file.xlsx
%GroupBy mounted file.xlsx
\caption{\label{fig:mns} \footnotesize \it \gb\ and \pr\ performance
  on a single node of Cori.}
\end{figure*}

\begin{figure*}[!htb]
\hspace{1.5in}
\begin{tabular}{c}
\begin{minipage}{3.5in}
\centerline{\includegraphics[keepaspectratio,height=2in]{cori-groupby-scale}}
\end{minipage} \\
\begin{minipage}{3.5in}
\centerline{\includegraphics[keepaspectratio,height=2in]{cori-groupby-scale-standalone}}
\end{minipage} \\
\end{tabular}
% cori-groupby-yarn.xlsx
% cori-groupby-standalone.xlsx
\caption{\label{fig:cg} \footnotesize \it GroupBy weak scaling on Cori up
  to 8 nodes (256 cores). Top: with YARN. Bottom:
  with the Spark standalone scheduler. }
\vspace{-.1in}
\end{figure*}

\begin{figure*}[!htb]
\hspace{1in}
\begin{tabular}{c}
\begin{minipage}{3.5in}
\includegraphics[keepaspectratio,height=2.4in]{cori-pagerank-scale}
\end{minipage} \\
\begin{minipage}{3.5in} 
\includegraphics[keepaspectratio,height=2.4in]{cori-bigd-scale}
\end{minipage} \\
\end{tabular}
% cori-pagerank-yarn.xlsx
% cori-bigdatabenchmark-yarn.xlsx
\caption{\label{fig:prbd} \footnotesize \it \pr\ and \bd\ scaling on
  Cori, up to 8 nodes (256 cores). }
\vspace{-.2in}
\end{figure*}

In Figure~\ref{fig:mns} we show the single node performance on Cori in
all configurations. As shown, using the back-end Lustre file system is
the slowest, by as much as $7\times$ when compared to the best
configuration.  Both file system configurations 
improve performance significantly by reducing the overhead of calls to
open files: \rdisk\ is up to $\approx 7.7\times$ faster and \lfs\ is
$\approx 6.6\times$ faster than Lustre. 

\fpool\ also improves performance in
all cases. It is $\approx 2.2\times$ faster than Lustre, and
interestingly enough is speeds up the other two configurations. For
example, for \gb\ where each task performs  $O(partitions^2)$ file
opens, adding pooling to the ``local'' file system
(e.g. \rdisk+\fpool) improves performance by $\approx 15\%$. The
performance improvements are attributed to the lower number of {\tt
  open}  system calls. For \pr\ and \bd\ the
improvements are a more modest 1\% and 2\% respectively.  As it never degraded
performance, this argues for running in configurations where our
\fpool\ implementation itself  or a user level file system is
interposed between Spark and any other ``local'' file systems used for
shuffle data management.  

For all configurations the performance improvements are proportional to the number of file
opens during the shuffle stage: \gb\ is quadratic in partitions while
in \pr\ it is a function of the graph structure. 

In Figure~\ref{fig:cg} we show the scalability of \gb\ up to eight nodes (256 cores).  We present the
average task time and within it, distinguish between time spent in
serialization (Serialization), disk access together with network
access (Fetch) and application level computation (App). 
\rdisk\ is
fastest, up to $6\times$ when compared to Lustre. \fpool\ is slower
than \rdisk, but still significantly faster than Lustre, up to
$4\times$. The performance differences between \rdisk\ and \fpool\
increase with the scale: while system call overhead is constant,
metadata latency performance degrades.  When combining \fpool\ 
with \lfs\ we observe performance improvements ranging from  17\% on one node to  2\%
on 16 nodes.

In Figure~\ref{fig:prbd} we present scalability for \pr\ (left) and
\bd\ (right). As mentioned, the inputs for these benchmarks are not
very large and we scale up to 8 nodes. The trends for \pr\ are similar
to \gb\ and we observe very good performance improvements from \fpool\
and \rdisk. The improvements from combining pooling with \rdisk\ are
up to 3\%.  In addition, when strong scaling \pr\ the performance of
\rdisk\ improves only slightly with scale (up to 25\%), while configurations that touch the
file system (Lustre and \bb) improve by as much as $3.5\times$. The gains are explained by better parallelism in the read/write
operations during shuffle.

The performance of \bd\ is least affected by any of our
optimizations. This is because behavior is dominated by the initial
application level  I/O stage, which we did not optimize. This is the
case where \rdisk\ helps the least and further performance
improvements can be had only by applying the file pooling optimization
or \lfs. \bd\ illustrates the fact that any optimizations have to
address in shuffle in conjunction with the application level I/O.

When using the Yarn resource manager we could not effectively scale
Spark up to more than 16 nodes on either Edison or Cori. The
application runs but executors are very late in joining the job and
repeatedly disappear during execution. Thus the execution while
reserving the initially requested number of nodes, proceeds on far
fewer. After exhausting timeout configuration parameters, we are still
investigating the cause. 

For larger scale experiments we had to use the Spark standalone
scheduler, results presented in Figure~\ref{fig:cg} right. While Yarn runs one executor (process) per node, the Spark
manager runs one executor per core.  The Lustre
configuration stops scaling at 512 cores.  The standalone scheduler limits the the performance impact
of our file pooling technique: with Yarn we provide a per node
cache while with the standalone scheduler we provide a per core
cache. This is reflected in the results: while with YARN \fpool\ scales
similarly to \rdisk, it now scales similarly to Lustre and we observe
speedup only as high as 30\%. Note that
\fpool\ can be reimplemented for the standalone scheduler, in which
case we expect it to behave again like \rdisk.

As illustrated in Figure~\ref{fig:cgs} we successfully (weak) scaled \rdisk\ up to 10,240
cores. Lustre does not scale past 20 nodes, where we start observing
failures and job timeouts. When running on the \bb\ we observe
scalability up 80 nodes (2,560 cores), after which jobs abort. Note
that  \bb\ performance is highly variable at scale and we
report the best performance observed across all
experiments.

Figure~\ref{fig:lustremount} compares Lustre, \rdisk\ and \lfs. To use \lfs\ on more than one node, we run Spark inside a Shifter user-defined image. With Shifter, each node mounts a single image containing JVM and Spark installations in read-only mode and a per-node read/write loopback file system. Because the JVM and Spark are stored on a file-backed filesystem in Shifter, file opens required to load shared libraries, Java class files, and Spark configuration files are also offloaded from the metadata server, improving performance over configurations where Spark is installed on the Lustre filesystem. Identically configured GroupBy benchmarks running on \rdisk\ with Spark running in Shifter is up to 16\% faster than than with Spark itself installed on Lustre.
In addition, since the mount is private to a single node, the kernel buffer 
cache and directory entry cache can safely cache metadata blocks and 
directory entries.  This can significantly reduce the number of metadata 
operations and improves performance for small I/O operations.
For the \lfs\ implementation in Shifter initializes a sparse file in the Lustre file
system for each node in the Spark cluster.  These files are then formatted as
XFS file systems and mounted as a loop back mount during job launch.
Unlike using \rdisk, the \lfs\ approach is not limited to the memory size 
of the node and it doesn't take away memory resources from the application.  
Using \lfs\ we can scale up to 10,240 cores, with time to completion
only 13\% slower than \rdisk\ at 10,240 cores.



\begin{figure*}
\begin{tabular}{cc}
\begin{minipage}{3.5in}
\centerline{
\includegraphics[keepaspectratio,height=1.5in]{cori-groupby-large}}
\caption{\label{fig:cgs} \footnotesize \it \gb\ at large
  scale on Cori, up to 320 nodes (10,240 cores). Standalone scheduler. Series
  ``Slowdown'' shows the slowdown of \bb\ against \rdisk, plotted
  using the secondary right axis.}
\end{minipage}&
\begin{minipage}{3.5in}
\vspace{-.2in}
\centerline{
\includegraphics[keepaspectratio,height=1.5in]{cori-groupby-large-lustremount}}
\caption{\label{fig:lustremount} \footnotesize \it \gb\ at large
  scale on Cori, up to 320 nodes (10,240 cores). Standalone scheduler. Lustre, \rdisk, and \lfs.}
\end{minipage} \\
\end{tabular}
\end{figure*}

	
	

