\eject
\section{Improving Shuffle Scalability With Better Block Management}
\label{sec:iterative}

 Even when running using a good configuration available, e.g. \fpool+\rdisk, some algorithms may not
 scale due to the memory management  within the shuffle manager, which
 introduces excessive vertical data movement. The
 behavior of the PageRank algorithm illustrates this. 

\begin{figure*}[!htb]
\begin{tabular}{cc}
\begin{minipage}{3.5in}
\includegraphics[width=\columnwidth,keepaspectratio]{prank}
\end{minipage} &
\begin{minipage}{3.5in}
\includegraphics[width=\columnwidth,keepaspectratio]{prank-constr}
\end{minipage} \\
\end{tabular}
\caption{	\label{fig:prankstat} \footnotesize \it \pr\
  performance on a single node of Edison. The amount of memory used
  during execution is plotted against the right hand side axis. The
  time taken by each iteration is plotted against the left hand side
  axis. Execution under constrained memory resources slows down with the
  number of iterations.}
\end{figure*}

\begin{figure*}[!htb]
\begin{tabular}{cc}
\begin{minipage}{3.5in}
\includegraphics[width=\columnwidth,keepaspectratio]{prank-mem}
\end{minipage} &
\begin{minipage}{3.5in}
\includegraphics[width=\columnwidth,keepaspectratio]{prank-constr-mem}
\end{minipage} \\
\end{tabular}
\caption{	\label{fig:prankstatmem} \footnotesize \it \pr\ IO
  behavior on a single node of Edison. The amount of memory used
  during execution is plotted against the right hand side axis.  The
  amount of bytes read and written from disk is plotted against the
  left hand side axis. While memory usage stays constant, the amount
  of bytes read explodes under constrained memory resources.}
\end{figure*}

In Figure~\ref{fig:prankstat} left we show the evolution of the algorithm
for a problem that fits entirely in main memory on one node of
  Edison. We plot both memory
usage and the duration of an iteration over the execution. As shown,
execution proceeds at a steady rate in both memory and time.  On the
right hand side of the figure, we plot the evolution of the algorithm
when the working set does not fit in the main memory. As illustrated,
each iteration becomes progressively slower and each iteration takes
double the amount of its predecessor. The same behavior is observed on
the workstation, albeit less severe.

After
investigation using the instrumentation framework already developed,
we observed that during constrained execution the amount of data read
from disk grows at a rate two orders of magnitude higher than during
unconstrained execution.  After further investigation, we attributed  the root cause
of the problem to the shuffle block manager. Whenever running out of
memory, the block manager evicts the least recently used block.
The first subsequent access to the evicted block triggers
recomputation, which evicts another block needed for the partial
solution which in turn triggers recomputation and eviction of blocks
needed. This results in orders of magnitude increases in vertical data
movement, as illustrated in Figure~\ref{fig:prankstatmem}.

This behavior affects the scaling of iterative algorithms on all
systems and it should be fixed. In the data
center it is less pronounced as local disks are better at latency.  As
shown, it is very pronounced on our HPC systems. One  lesson here is that because storage behaves differently, in
  particular for small requests, there exists incentive to
  specifically tune the shuffle block manager for HPC.

 For the
PageRank algorithm we have actually an algorithmic fix which involves
marking as persistent  the intermediate result RDDs from each iteration. This causes Spark
to write them to the back-end storage. Upon eviction, a persistent
block is read from storage instead of being
recomputed. Figure~\ref{fig:prankfixed} shows the performance of the
fixed PageRank algorithm and we observe performance improvements as
high as $11\times$. Note that all the performance findings in this
paper are reported on this fixed algorithm. The original GraphX
implementation  does not scale beyond a single node on our systems.


\begin{figure*}[!htb]
\begin{tabular}{ccc}
\begin{minipage}{2.2in}
\includegraphics[width=\columnwidth,keepaspectratio]{prank-partu}
\end{minipage}&
\begin{minipage}{2.2in}
\includegraphics[width=\columnwidth,keepaspectratio]{prank-partc}
\end{minipage}&
\begin{minipage}{2.2in}
\includegraphics[width=\columnwidth,keepaspectratio]{prank-fixed}
\end{minipage} \\

\end{tabular}
\caption{	\label{fig:prankfixed} \footnotesize \it  Number of
  partitions read during the shuffle stage for \pr\.\ Left: execution
  with unconstrained memory. Right: when memory is constrained the
  number of partitions read from disk is one order of magnitude
  larger. Right: persisting intermediate results fixes the performance
problems and we see a reduction by a order of magnitude in partitions
read from disk. }
\end{figure*}


There are two possible generic solutions to this problem. First, we could
implement a system which tracks how often shuffle data must be reread from
disk and automatically persist partitions that depend on that data when
a threshold is exceeded. Second, we could track the cost of recomputing
and rereading the lineage of an RDD and, rather than evicting on a
least-recently-used basis, instead evict the block which will have the
lowest recompute or reread cost.

Note that we were initially interested in evaluating the \texttt{spark-perf}
machine learning benchmark suite~\cite{spark_perf} for this study.
Due to this problem with iterative algorithms, we postponed the
evaluation to the time when we can consider the aforementioned fix
in the shuffle manager.