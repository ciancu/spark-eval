\section{Scaling Concerns}


On a data center system architecture with local disks, one does not
expect file open (or create)  time to have a large effect on the
overall time to job completion. Thus, Spark and the associated domain
libraries implement stateless operation for resilience and
elastic parallelism purposes by 
opening and closing the  files involved in  each individual data access: {\it  file
metadata operations are a scalability bottleneck on our HPC
systems.} Any effort scaling Spark up and out on an HPC installation
has first  to address  this concern. 


There are several Spark configuration alternatives that affect file I/O
behavior. We were first interested to determine if the
larger number of cores in a HPC node allows for a degree of
oversubscription (partitions per core) high enough to hide the MDS
latency.   We have systematically explored
consolidation, speculation, varying the number of partitions and data
block sizes to no avail.

In the time honed HPC tradition, one solution is to throw bigger and
better hardware at the problem.   The first aspect is to exploit the
higher core concurrency present in HPC systems. As the previous
Section shows, increasing\footnote{Cori Phase II will contain Intel
  Xeon Phi nodes with up to 256 cores per node. This will become
  available circa Oct 2016 to early users.}  the number of cores per node does improve
performance, but not enough to mitigate the effects of the file
system.

 For the Lustre
installations evaluated, metadata performance is determined by the
MDS hardware configuration. Although Cori contains
multiple MDSes, the current Lustre 2.6 version does not exploit them
well\footnote{Supports a restricted set of operations that are not
  frequent in Spark.} and performance for the Spark workload is identical to that of a
single MDS.  When comparing Cori with Edison, the former
contains newer hardware and exhibits lower metadata access latency
(median 270$\mu s$ on Cori vs 338$\mu s$ on Edison),
still when using the full node (32 and 24 cores)  both are at best
50\% slower than a eight core workstation.  Enabling  multiple
MDSes will improve scalability but not the latency of an  operation~\cite{indiana}, thus over-provisioning the Lustre metadata service is
unlikely to provide satisfactory per node performance.

A third hardware solution is provided by the \bb\  I/O
subsystem installed in Cori. This large NVRAM array situated close to
the CPU is designed to improve throughput for small I/O operations and
for pre-staging of data. The question still remains if it is well
suited for the access patterns performed by Spark. 

Besides hardware, software techniques can alleviate some of the
metadata performance bottlenecks. The first and most obvious solution
is to use a memory mapped file system (e.g. {\tt /dev/shm}) as the
secondary storage target for Spark. Subject to physical memory
constraints, this will eliminate a large fraction of the traffic to
the back-end storage system. In the rest of this paper, we will refer
to this configuration as \rdisk. Note that this is a user level
technique and there are several limitations: 1) the job crashes when
memory is exhausted; and 2) since data is not written to disk it does not provide any resilience and persistence guarantees.

HPC applications run in-memory so it may seem that \rdisk\ provides a
solution. 
For medium to large problems and long running iterative algorithms Spark will
fail during execution when using \rdisk, due to lax garbage collection
in the block and shuffle managers.  To accommodate large problems we
evaluate a configuration where a local file system is mounted and
backed by a Lustre file, referred to as \lfs. This requires administrative privilege on the
systems and due to operational concerns we were initially granted access to only
one node. 
Based on the results of this study, this capability was added to Shifter~\cite{shifter}, which is
NERSC developed software that enables Docker containers to be run on shared
HPC systems.


To understand scaling with large problems  we develop a
software caching layer for the file system metadata, described in
Section~\ref{sec:fp}. In the rest of this paper we refer to this
configuration as \fpool. This is a user level approach  orthogonal to
the solutions that mount a local file system. Since data is stored on
Lustre, \fpool\ provides resilience and persistence guarantees. 

%\eject
\subsection{I/O Scaling in Spark} 


I/O overhead occurs due to metadata operations, as well as proper data
access read/write operations.  All these operations occur in both
the application level I/O, as well as inside Spark  for memory
constrained problems or during the shuffle
stage.

In Section~\ref{sec:single} we have illustrated the impact of {\tt
  fopen} metadata operations on the performance of \bd.. There, the benchmark performed during the application input
stage a number of open operations linear in the number of partitions
$O(partitions)$.  Big Data Benchmark did not involve a large amount of shuffle
data. 

Because Spark allows partitions to be cached in memory, slow reading
of the initial data is not necessarily problematic, particularly in
an interactive session in which multiple queries are being performed
against the same data. Assuming that the working set fits in memory,
disk access for input data can be avoided except for the first
query. In this case, the \bb\ can be also used for data pre-staging. 



\begin{figure}[!htbp]
\centering
\includegraphics[height=1.8in,keepaspectratio]{./figures/groupby-scaling-overall}
\caption{\label{fig:groupby-scaling-overall} \footnotesize \it Time
  for the map and reduce phases of  GroupBy  on Edison for
  Lustre and \rdisk\  as we use additional nodes to process larger
  datasets (weak scaling).}	
%GroupBy scaling Edison.xlsx
\vspace{-.2in}
\end{figure}
	

In Figure~\ref{fig:groupby-scaling-overall} we show the scalability of
the GroupBy benchmark up to 16 nodes (384 cores) on Edison for a weak
scaling experiment where the problem is chosen  small enough to fit entirely in memory.

 GroupBy 
measures worst-case shuffle performance: a wide shuffle in which every
partition must exchange data with every other partition. The benchmark
generates key-value pairs locally within each partition and then
performs a shuffle to consolidate the values for each key. The shuffle
process has two parts: in the first (map) part, each node sorts the
data by key and writes the data for each partition to a
partition-specific file. This is the \emph{local} task prior to the
stage boundary in Figure~\ref{fig:job-stage-partition}. In the second
(reduce) part, each node reads locally-available data from the
locally-written shuffle files and issues network requests for
non-local data. This is the \emph{global} task after the stage
boundary in Figure~\ref{fig:job-stage-partition}. 


When running entirely in memory (\rdisk)
performance scales with nodes, while scalability is poor when using
Lustre. As illustrated, the Map phase scales on Lustre, while the
Shuffle phase does not. For reference, on the workstation, mean task duration is 1,618 ms for \rdisk\ and 1,636 ms for local disk. On the Edison node, mean task duration was 1,540 ms for \rdisk\ and 3,228 ms for Lustre. 

We instrumented Spark's Shuffle Manager component to track file I/O
operations. During the write phase of the shuffle, a shuffle file is
created for each partition, and each shuffle file is written to as
many times as there are partitions. An index file is also written,
which contains a map from keys to a shuffle file and offset. During
the read phase, for each local partition to read and each remote
request received, the index file is opened, data is read to locate
the appropriate shuffle data file, which is then opened, read, and
closed. The number of file open operations during the shuffle is
quadratic in the number of partitions $O(partitions^2)$.
	
To enable load balancing, the Spark documentation suggests a default number of partitions as 4x
the number of cores. On 16  nodes of Edison, with a total of 384
cores, then, we have 1,536 partitions, giving us 1,536 shuffle data files,
each of which is opened 1,536 times during the write phase and another
1,536 times during the read phase, resulting in 4,718,592 file open.
Not only is the number of file opens is quadratic in partitions, but the cost \emph{per} file
open also grows as we add nodes, as shown in
Figure~\ref{fig:open-time-scaling}.

As the number of file I/O operations is linear with the number of
partitions/cores during the application I/O and quadratic during the
shuffle stage, in the rest of paper we concentrate the evaluation on
the shuffle stage. 
	
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.9\columnwidth,keepaspectratio]{./figures/open-time-scaling}
	\caption{	\label{fig:open-time-scaling} \footnotesize
          \it Average time for a  {\tt open}, {\tt read}, and {\tt
            write} operation performed during the \gb\ execution with
          weak scaling on Cori.}
%IO Time Combined.xlsx
\vspace{-.2in}
\end{figure}
	


As for each read/write operation Spark will perform a file open, the
performance ratio of these operations is an indicator of scalability. 
Figure~\ref{fig:open-read-close} shows the
performance penalty incurred by repeatedly opening a file, performing
one read of the indicated size, and closing the file, versus opening
the file once, performing many reads of the indicated size, and
closing the file. Using many open-read-close cycles on a workstation
with a local disk is $6\times$ slower for 1 KB reads than opening once
and performing many reads, while on Edison with Lustre, many
open-read-close cycles is $56\times$ slower than opening once and
reading many times. Lustre on Cori is similar, while the Burst Buffers
in striped mode reduce the penalty to as low as $16\times$. All of
the filesystems available on our HPC systems incur a substantial penalty
from open-per-read.

The scalability problems caused by the large number of file opens are
exacerbated by the potentially small size of each read. Many data
analytics applications have a structure in which many keys are
associated with a small number of
values. For example in PageRank, most write operations 
are smaller than 1KB. This reflects the structure of
the data, as most websites have few incoming links. The data is
structures as key-value pairs with a site's URL as the key and a list
of incoming links as the value, so most values are short.

\comment{	
\begin{figure*}[!htbp]
\centering
\includegraphics[width=0.9\textwidth,height=3in]{./figures/pagerank-block-sizes}
\caption{Number of writes of a given size range during shuffling in PageRank.}
\label{fig:pagerank-block-sizes}
\end{figure*}
}
	

	
\begin{figure}[!htbp]
\centering
\includegraphics[width=0.9\columnwidth,keepaspectratio]{./figures/open-read-close}
\caption{	\label{fig:open-read-close} \footnotesize \it
  Performance improvements from amortizing the cost of file opens. We
  compare one read per open with 100,000 reads per open.}
\end{figure}
	







\subsection{Improving Metadata Performance With File Pooling}
\label{sec:fp}

For problems small enough to fit in the main memory, the \rdisk\ Spark
configuration scales. However, in our
experiments many large problems ran out of memory at runtime,
particularly iterative algorithms where the block garbage collection inside
the shuffle manager is not aggressive. 



In order to accommodate large problems at scale  we have simply chosen to add a
layer for pooling and caching open file descriptors within Spark. All
tasks within an Executor (node) share a descriptor pool. We redefine
{\tt FileInputStream} and {\tt FileOutputStream} to access the pool
for open and close operations. Once a file is opened, subsequent close
operations are ignored and the descriptor is cached in the pool. For
any subsequent opens, if the descriptor is available we simply pass it
to the application. To facilitate multiple readers, if a file is
requested while being used by another task, we simply reopen it and
insert it into the pool. 

This descriptor  cache is subject to capacity constraints as there are
limits on the number of {\tt Inodes} within the node OS image, as well
as site-wide Lustre limits on the number of files open for a given
job. In the current implementation, each Executor is assigned its
proportional number of entries subject to these constraints. 

We  evaluated a statically sized file pool
using two eviction policies to solve capacity conflicts: LIFO and
FIFO. For brevity we omit detailed results and note that LIFO provides
best performance for the shuffle stage. As results indicate, this simple implementation enables Spark to
scale.

Further refinements are certainly possible. Application
I/O files can be easily distinguished from intermediate shuffle
files and can be allocated from a smaller pool, using
FIFO. Within the shuffle, we can tailor the eviction policy
based on the shuffle manager behavior, e.g. when a block is dropped
from memory the files included in its lineage are likely to be
accessed together in time  during recomputation. 

Running out of {\tt Inodes} aborts execution so in our implementation
a task blocks when trying to open a file and the pool descriptor is
filled at capacity.  As this can lead to livelock, we have audited the
Spark implementation and  confirmed with traces that the  implementation paradigm
is to open a single file at a time, so livelock cannot occur.

