\section{Spark Architecture}
	Apache Spark~\cite{zaharia_spark:_2010} is an open-source data analytics framework, designed to operate on datasets larger than can be processed on a single node while automatically providing for scheduling and load-balancing. In this respect, it is like the Map-Reduce model~\cite{dean_mapreduce:_2008} found in systems such as Hadoop, providing an abstraction in which programs are expressed as data flow graphs, consisting of nodes which are of two types: \emph{map operations}, which are purely local, and \emph{reduce operations}, which can involve communication between node. The traditional MapReduce framework is limited, however, by its restriction that the graph be acyclic, preventing efficient representation of iterative methods, and by its use of redundancy to provide resiliency. Spark relaxes the requirement that the data flow graph be acyclic, and provides resiliency through \emph{resilient distributed datasets}~\cite{zaharia_resilient_2012}, or RDDs, which store data across nodes and which carry sufficient information to recompute their contents. Programs are expressed in terms of RDDs derived from transformations (of which map is only one) applied to other RDDs and actions (of which reduce is only one). The application developer can choose to request that certain RDDs be cached in memory or saved to disk. The developer therefore has to make decisions based on tradeoffs between the costs of storage (in memory and time) and recomputation (in time). RDDs are lazily evaluated, which can create challenges in attributing performance to particular lines or regions of code, as they do not execute until they are needed.
	
	The \emph{Master} node executes a driver program, which creates the data flow graph by applying transformations and actions to RDDs, and distributes ownership of partitions to worker nodes within the cluster. When the result of an uncomputed RDD partition is needed, a \emph{job} is created, consisting of multiple \emph{stages}. Within a stage, only intra-partition communication can occur. All inter-partition communication happens at stage boundaries, through a process called \emph{shuffling}, as shown in Figure~\ref{fig:job-stage-partition}. By deferring any computation until a result is needed, the scheduler can schedule work to compute only what is necessary to produce the result. In the event of the loss of a partition, only that partition needs to be recomputed. 
	
	\begin{figure*}[]
	\centering
	\includegraphics[width=0.8\textwidth,,keepaspectratio]{./figures/job-stage-partition}
	\caption{Decomposition of a job into stages and tasks on partitions, with inter-partition communication limited to stage boundaries.}
	\label{fig:job-stage-partition}
	\end{figure*}
	
	\fix{TODO: HDFS/storage architecture, YARN/Mesos}


\section{Data Movement in Spark}
\label{sec:datamovement}
One of the major performance influencing factors in spark is the data movement. This section overviews the data movement schemes in Spark, design constraints, and optimization techniques. 
Data could be in input from user, output of computation, or intermediate state used by the runtime. The runtime typically breaks large computations into manageable pieces by splitting data into partitions to improve load balancing. Partitions are further split into blocks to facilitate the data movement.
The typical performance challenge arises from the mismatch between the memory hierarchy the runtime is optimized for and the underlying architecture.
Spark adopts imperative programming model style, where the programmer expresses the required computation without specifying the mechanics of execution. As such, the computation decomposition and distribution, communication orchestration, load-balancing etc are all managed by the runtime. This model allows tuning effort with less application involvement.

Figure~\ref{fig:spark-storage-architecture} shows the Spark compute engine interaction with the block and shuffle managers, which control with data movement. 

\begin{figure*}[!htbp]
\centering
\includegraphics[width=0.8\textwidth,,keepaspectratio]{./figures/spark-storage-architecture}
\caption{Data movement in Spark and the interaction with the memory hierarchy.}
\label{fig:spark-storage-architecture}
\end{figure*}

\subsection{Data Objects and Naming}
User-level input data and output data typically have a global naming space to facilitate data storage, partitioning, and movement. A Large dataset is typically partitioned, while small data could either be replicated or broadcast upon request. Application generated RDD fall also into the globally addressable entities. 
Given that the spark runtime takes care of executing user actions by applying transformations into the application data, intermediate data are typically managed by the runtime.  The naming scope of these data objects is typically local.
Both locally named and globally named objects may exceed the capacity of the physical memory. As such, for efficient execution Spark requires efficient data movement for both locally named and global named objects. 
A typical challenge is managing these types naming schemes is that mismatch with underlying architectures. For instance, a global object may need to be distributed (partitioned) across multiple storage spaces. As such, a naming service is needed to locate the physical location of an object.  Conversely, locally named objected stored in a physically shared storage may experience undue contention while servicing requests.   

\subsection{Vertical Data Movement}
Vertical data movement refers to the movement through the memory hierarchy (including the persistent storage).  This type of data movement is typically needed to move input data blocks into the memory for processing and for storing output data to the persistent storage if needed.
Spark tries to minimize the vertical movement using the RDD abstraction, which allows persisting the data in the fast dynamic memory. Memory space limits the number of objects that the runtime could keep in memory. As such, the Spark runtime assigns the task of moving objects across the memory hierarchy to a block manager. Spark allows the application to define their persistence attributes to RDD to allow better optimization of the data movements across the memory hierarchy. 
Although the reduction of vertical data movement is one of the main design objectives of Spark, they are not avoidable in practice. The runtime level management of memory makes it harder to guarantee fitting data in the physical memory. As such spilling memory blocks into persistent storage, or block re-computation typically trigger superfluous vertical data movement. 

\subsection{Horizontal Data Movement -  Block Shuffling}
The horizontal data movement refers to the communication phase between compute nodes to shuffle the data. This cross node data movements are typical between computation stages where task assignment changes. 
Spark assigns the horizontal data movement to the shuffle manager and the block manager. A horizontal data movement request of a block could trigger a vertical data movement because a block may not be resident in memory.  
 The horizontal data movement typically relies on client server model, which is not very common in HPC platforms.  HPC runtime typically either use coordinated two-sided communication, collective, or one-sided communication. The data are typically resident in memory before initiating a data movement request.

\subsection{Storage Architecture and Data Movement}
Vertical data movement typically benefits from a local storage attached to compute nodes. The number of storage disks scales linearly with the number nodes involved in the computation. Their bandwidth also scale with the number of compute nodes. They typically do not have resiliency support. As such runtimes such as Hadoop, uses redundant copying to have resilience.  This type of storage exhibits lower latency for accessing data compared with network-based file system, such as Lustre. It also poses a challenge for providing a global name space.
Network-based file systems eliminate the need for a name service because files are globally visible by all compute nodes. They typically provide specialized high performance storage systems. They also provide resiliency features that eliminate the need for the runtime redundant copying of files.  These advantages come with an increase in latency to communicate with file servers to request file access or to communicate data. Given that these network file servers are shared between many concurrently scheduled applications, the servers typically optimize for overall system throughput. As such individual application may observe increase in latency and higher variability. 

Looking at the type of data objects managed by spark, we see that we need to combine the advantage of both architectures to achieve optimal performance. 
Globally objects, which are less frequently updated, could benefit from network-based file system, while local objects and frequently written one could benefit from locally attached file system.  



