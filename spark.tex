%\eject
\section{Spark Architecture}

Apache Spark~\cite{zaharia_spark:_2010} and Hadoop~\cite{hadoop} are  open-source data analytics frameworks, designed to operate on datasets larger than can be processed on a single node while automatically providing for scheduling and load-balancing.  They implement the Map-Reduce model~\cite{dean_mapreduce:_2008}  using an abstraction in which programs are expressed as data flow graphs.
The nodes in the graph are of two types: \emph{map operations}, which are purely local, and \emph{reduce operations}, which can involve communication between nodes. 

The traditional MapReduce framework~\cite{dean_mapreduce:_2008} is limited to acyclic graphs, preventing efficient representation of iterative methods, and  it uses data redundancy to provide resiliency. Spark can handle cyclic and acyclic graphs, and provides resiliency through \emph{resilient distributed datasets}~\cite{zaharia_resilient_2012} (RDD), which carry sufficient information (lineage) to recompute their contents. In particular, the ability to express iterative algorithms accelerated Spark's adoption. 

Programs are expressed in terms of RDDs derived from transformations of other RDDs (e.g. Map) and actions (e.g. Reduce). The application developer can choose to request that certain RDDs be cached in memory or saved to disk. The developer therefore has to make decisions based on tradeoffs between the costs of storage (in memory and time) and recomputation (in time). RDDs are lazily evaluated, which creates challenges~\cite{visibility}  in attributing performance to particular lines or regions of code, as they do not execute until they are needed.
	
In Spark, the \emph{Master} node executes a driver program, which creates the data flow graph by applying transformations and actions to RDDs, and partitions  ownership of data to worker nodes within the cluster. When the result of an uncomputed RDD partition is needed, a \emph{job} is created, consisting of multiple \emph{stages}. Within a stage, only intra-partition communication can occur. All inter-partition communication happens at stage boundaries, through a process called \emph{shuffling}, as shown in Figure~\ref{fig:job-stage-partition}. By deferring any computation until a result is needed, the scheduler can schedule work to compute only what is necessary to produce the result. In the event of the loss of a partition, only that partition needs to be recomputed.
	
\begin{figure*}[!htbp]
\begin{tabular}{cc}
\begin{minipage}{3in}
\includegraphics[width=3in,height=1.5in]{./figures/job-stage-partition}
\caption{\label{fig:job-stage-partition}\footnotesize \it Decomposition of a job into stages and tasks on partitions, with inter-partition communication limited to stage boundaries.}
\end{minipage} &
\begin{minipage}{3.75in}
\includegraphics[height=1.5in]{./figures/spark-storage-architecture}
\caption{\label{fig:spark-storage-architecture} \footnotesize \it Data movement in Spark and the interaction with the memory hierarchy.}
\end{minipage} \\

\end{tabular}

\end{figure*}
	

\subsection{Data Movement in Spark}
\label{sec:datamovement}

Data movement is one of the performance determining factors in any large scale system. 
In Spark, data is logically split into {\it partitions}, which have an  associated worker task. A partition is subdivided into {\it blocks}: a block is the unit of data movement and execution.  Figure~\ref{fig:spark-storage-architecture} shows  the interaction of the Spark compute engine  with the block and shuffle managers, which control  data movement. The BlockManager handles application level input and output data, as well as intermediate data within the Map stages. The ShuffleManager handles runtime intermediate results during the shuffle stage.

\begin{figure*}[!htbp]
\begin{tabular}{ccc}
\begin{minipage}{1.3in}
\includegraphics[height=1.5in]{./figures/lustre-layout}
%\includegraphics[height=1.5in]{./figures/bb}
\caption{\label{fig:lustre} \footnotesize \it Lustre architecture. (Courtesy of Intel Wiki.)}
\end{minipage} &
\begin{minipage}{2.5in}
\includegraphics[height=1in,keepaspectratio]{./figures/blade} 
\caption{\label{fig:bb1} \footnotesize \it \bb\ node architecture.(Courtesy of NERSC.)}
\end{minipage} &
\begin{minipage}{2.5in}
\hspace{.2in}
\includegraphics[height=1.5in,keepaspectratio]{./figures/bb}
\caption{\label{fig:bb} \footnotesize \it \bb\ topology. (Courtesy of NERSC.)}
\end{minipage} \\
\end{tabular}
\vspace{-.1in}
\end{figure*}


\parah{Data Objects and Naming} Spark manipulates data with global scope, as well as local scope. Application level data (RDDs) are using a global naming space, intermediate data blocks generated throughout execution have  a local scope and naming scheme. 
Objects may exceed the capacity of the physical memory and need to be efficiently moved through the  storage hierarchy;  the  typical challenge when managing  naming schemes is  mismatch with underlying system  architecture. For instance, when  global object is distributed (partitioned) across multiple storage spaces a long latency naming service may be needed to locate its physical location.  Conversely, any locally named object stored in a physically shared storage may experience undue contention while servicing requests.   A current research direction in the Spark community is providing an efficient global naming service, which can reduce network traffic. Note that the global file system in HPC installations provides global naming. 

\parah{Vertical Data Movement} Vertical data movement refers to the movement through the entire memory hierarchy, including persistent storage.  It  is needed to move input data blocks into the memory for processing and for storing output data to the persistent storage.
To minimize vertical movement for RDDs, Spark allows persisting data in the fast level of memory. As fast memory is capacity constrained, the Spark runtime assigns the task of moving objects across the memory hierarchy to a block manager. Whenever the working set size (input data or intermediate results) exceeds memory capacity, the block manager may trigger vertical data movement. The block manager may also decide to drop a block, in which case its later access may trigger additional vertical data movement for recomputation. Research efforts such as Tachyon~\cite{tachyon} aim to reduce expensive (to storage) vertical data movement by replacing it with horizontal (inter-node) data movement. In network-based storage systems, a critical~\cite{alam_parallel_2011,carns_small-file_2009} component to the performance of vertical data movement is the file setup stage (communication with the metadata servers). 

\parah{Horizontal Data Movement -  Block Shuffling} The horizontal data movement refers to the shuffle communication phase between compute nodes. Spark assigns the horizontal data movement to the shuffle manager and the block manager. A horizontal data movement request of a block could trigger a vertical data movement because a block may not be resident in memory.   
Optimizing the performance of horizontal data movement has been the subject of multiple studies~\cite{wang_hadoop_2011, Islam12,lu_accelerating_2014}, in which hardware acceleration such as RDMA is used to reduce the communication cost.  The benefit of these techniques is less profound on HPC systems with network-based storage~\cite{sparks_cray_15} because the performance is dominated by vertical data movement. 


\subsection{System Architecture and Data Movement} Data centers have local storage attached to compute nodes. This enables fast vertical data movement and the number of storage disks scales linearly with the number nodes involved in the computation. Their bandwidth also scale with the number of compute nodes.  The archetypal file system for data analytics is the Hadoop Distributed File System (HDFS) which aims to provide both fault tolerance and high throughput access to data.  HDFS implements a simple coherency for write-once-read-many file access, which fits well the Spark and Hadoop processing models.  In Spark with HDFS, global naming services are implemented in a client-server paradigm. A request is generated for the object owner, subject to the network latency. The owner services it, maybe subject to disk latency (or bandwidth) and the reply is subject to network latency (or bandwidth). Vertical data transfers access the local disk.  Horizontal data are subject to network latency/bandwidth, as well as disk latency/bandwidth. 

HPC systems use dedicated I/O subsystems, where storage is attached to a ``centralized'' file system controller. Each and all nodes can see the same amount of storage, and bandwidth to storage is carefully provisioned for the system as a whole.
 Given that these network file servers are shared between many concurrently scheduled applications, the servers typically optimize for overall system throughput. As such individual applications may observe increase in latency and higher variability. The Lustre~\cite{braam_lustre_2004} architecture, presented in Figure~\ref{fig:lustre} is carefully optimized for throughput and implements a generic many-write-many-read coherency protocol. The installation consists of clients, a Metadata service (MDS) and Object Storage service. The Metadata service contains Metadata Servers, which handle global naming and persistence and the Metadata Targets which provide the actual metadata storage (HDD/SSD).  In Spark with Lustre, global naming services access the metadata servers and are subject to network latency and MDS latency. Most existing Lustre installations in production (prior to Lustre 2.6) use a single MDS, only very recent installations~\cite{cori,indiana} use multiple MDSes for improved scalability. Vertical data transfers are served by the Object Storage service, which contains the object Storage Server (OSS) and the Object Storage Target (OST), the HDD/SSD that stores the data. Bandwidth is provisioned in large scale installations by adding additional OSSes.

In our quest to introduce Spark into the HPC community there are two main questions to answer.

1. {\it  How does the differences in architecture between data centers and HPC influence performance? }
Previous performance studies of Spark in data center environments~\cite{roman2015understanding} indicate that its performance is dominated by the network, through careful optimizations to minimize vertical data movement and maximize the memory resident working set. 	Ousterhout et al.~\cite{ousterhout_making_2015} analyzed the performance of the Big Data Benchmark~\cite{uc_berkeley_amplab_bdb} on 5 Amazon EC2 nodes, for a total of 40 cores, and the TPC-DS benchmark~\cite{poess_tpc-ds_2002} on 20 nodes (160 cores) on EC2. These benchmarks both use Spark SQL~\cite{armbrust_spark_2015}, which allows SQL queries to be performed over RDDs. By instrumenting the Spark runtime, they were able to attribute time spent in tasks to several factors, including network and disk I/O and computation. They found that, contrary to popular wisdom about data analytics workflow, that disk I/O is not particularly important: when all work is done on disk, the median speedup from eliminating disk I/O entirely was only 19\%, and, more importantly, when all RDDs are persisted to memory, only a 2-5\% improvement was achieved from eliminating disk I/O. Upon introduction to HPC systems, we similarly need to understand whether access to storage or network performance dominates within Spark. 

2. {\it  What HPC specific features can we exploit to boost Spark performance?} Previous work optimizing data analytics frameworks on HPC systems~\cite{lu_accelerating_2014,Islam12}  proposes moving away from the client-server distributed paradigm and exploiting the global file name space already available or Remote Direct Memory Access (RDMA) functionality. Upon introduction to HPC systems, we are interesting in evaluating the potential for performance improvement of adopting such techniques into Spark. Besides providing an initial guide to system researchers, we are also interested in providing configuration guidelines to users and system operators. 


We explore these questions using three benchmarks selected to cover the performance space: 1) \bd\ uses SparkSQL~\cite{armbrust_spark_2015} and stresses vertical data movement; 2) \gb\ is a core Spark benchmark designed to capture the worst case scenario for shuffle performance, it stresses both horizontal and vertical data movement; and 3) \pr\ is an iterative algorithm from GraphX~\cite{gonzalez_graphx:_2014} and stresses vertical data movement. 